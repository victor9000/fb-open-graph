# fb-open-graph
A few concerte examples of using the Open Graph protocol to control how content is shared on Facebook.  Each example is implemented using meta tags, look in the head section of each html file for the specific implementation.

This repo is hosted using gh-pages.  This means that you can paste any of the following links into the open graph debugger, or into a status update, for a live preview.  Be sure to set your target audience to "Only Me" when you do live testing so you don't spam your feed with test posts.

| object | url |
|---|---|
| article | http://victor9000.gitlab.io/fb-open-graph/article-large.html |
| music.song | http://victor9000.gitlab.io/fb-open-graph/music.song-large.html |

Also, be sure to read through the [official documentation](https://developers.facebook.com/docs/sharing/webmasters).
